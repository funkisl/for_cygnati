import {CanActivate, Router} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {UserProfile} from './profile.model';
import {CookieService} from "ngx-cookie-service";
import {GlobalConst} from '../../globals/GlobalConst';
import {RouterConst} from '../../globals/RouterConst';



@Injectable()
export class Authentication implements CanActivate {

  userProfile: UserProfile = new UserProfile();


  constructor(private cookies: CookieService,
              private router: Router) {
  }

  canActivate() {
    if (((GlobalConst.TOKEN  === undefined) || GlobalConst.TOKEN.trim() === '')
      && ((this.cookies.get(GlobalConst.COOKIES_TOKEN) === undefined) || this.cookies.get(GlobalConst.COOKIES_TOKEN).trim() === '')) {
      this.router.navigate(['/'  + RouterConst.LOGIN]);
    }
    return true;
  }

  get authenticated(): boolean {
    if (((GlobalConst.TOKEN === undefined) || GlobalConst.TOKEN.trim() === '')
      && ((this.cookies.get(GlobalConst.COOKIES_TOKEN) === undefined ) || this.cookies.get(GlobalConst.COOKIES_TOKEN).trim() === '')) {
      return false;
    }
    return true;
  }

  get getProfile(): UserProfile {
    // If authenticated, set local profile property and update login status subject
    if ((this.authenticated) && (localStorage)) {
      if (JSON.parse(localStorage.getItem('profile'))) {
        this.userProfile = JSON.parse(localStorage.getItem('profile'));
      }
      else {
        //LAO костыль для удаляения печеньки если она каким-то чудом сохранилась по пути /main-page
        //так же костыль на то что без указания пути, печенька не удляется
        //Пока не ясно почему путь '/' меняется.
        //ссылка на проблему https://github.com/7leads/ngx-cookie-service/issues/5
        /****/

        this.cookies.delete(GlobalConst.COOKIES_TOKEN);
      //  this.cookies.removeItem(GlobalConst.COOKIES_TOKEN, '/main-page');
        this.canActivate();
      }
    }

    return this.userProfile;
  }
}

@Injectable()
export class Authentication4Login implements CanActivate {

  constructor(private cookies: CookieService,
              private router: Router) {
  }

  canActivate() {

   if (((GlobalConst.TOKEN === undefined) || GlobalConst.TOKEN.trim() === '')
      && ((this.cookies.get(GlobalConst.COOKIES_TOKEN )=== undefined) || this.cookies.get(GlobalConst.COOKIES_TOKEN).trim() === '')) {
     console.log(GlobalConst.TOKEN );
      return true;
    }
    this.router.navigate(['/' + RouterConst.HOME]);
  }
}
