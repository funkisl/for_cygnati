export class BasketModel {
  id: number;
  item: string;
  date: string;
  type: string;
  description: string;
  amount: number;
  cost: number;
}


export const TYPESELECT = [
  {code: 0, codeLat: "CONFIRM", text: "CONFIRM"},
  {code: 1, codeLat: "WAIT", text: "WAIT"},
  {code: 2, codeLat: "SEND", text: "SEND"},
  {code: 3, codeLat: "RECEIVED", text: "RECEIVED"},
  {code: 4, codeLat: "CLOSED", text: "CLOSED"},
  {code: 5, codeLat: "REJECTED", text: "REJECTED"}];
