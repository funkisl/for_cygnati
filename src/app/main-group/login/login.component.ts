import {Component, OnInit, Inject} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {FormGroup, Validators, FormBuilder, FormsModule} from '@angular/forms';
import {AuthorizeConst} from '../../globals/AuthorizeConst';
import {HttpRequestsService} from '../../services/http-requests/http-requests.service';
import {GlobalConst} from '../../globals/GlobalConst';
import {CookieService} from "ngx-cookie-service";
import {RouterConst} from '../../globals/RouterConst';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  deactivate = false;
  myForm: FormGroup;
  userNameError = 'Имя пользователя не соответствует шаблону';
  userPassError: string = 'Длина пароля меньше ' + AuthorizeConst.minPassLength + ' символов';

  authorized = true;
  userServerError: string;
  initials: string;

  submitAttempt = false;

  constructor(private httpRequest: HttpRequestsService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private cookies: CookieService,
              private location: Location,
              private fBuilder: FormBuilder) {
    this.myForm = fBuilder.group({
      'userName': ['', [Validators.pattern(AuthorizeConst.emailPattern)]],
      'userPass': ['', [Validators.minLength(AuthorizeConst.minPassLength)]],
    });
  }

  private setToken(securityToken: string) {
    GlobalConst.TOKEN = securityToken;
    //this.cookies.set(GlobalConst.COOKIES_TOKEN, securityToken, 0, '/');
    this.cookies.set(GlobalConst.COOKIES_TOKEN, securityToken);
  }

  private _setSession(authResult, profile) {
    // Save session data and update login status subject
    localStorage.setItem('profile', JSON.stringify(profile));
  }

  ok(data: any) {
    this.authorized = true;
    this.initials = data.payload.personName;
    localStorage.setItem('initials', this.initials);
    this._setSession(data.result, data.payload);
    this.setToken(data.securityToken);

     this.router.navigate(['/' + RouterConst.HOME],{relativeTo: this.activatedRoute});
  }

  bad(data: any) {
    console.log('login error', data);

    this.authorized = false;
    this.userServerError = /*data*/ 'Пользователь или пароль не верен';
    this.myForm.controls.userPass.reset();
  }

  submit() {
    this.deactivate = !this.deactivate;

    this.submitAttempt = true;

    if (this.myForm.invalid) {
      this.deactivate = !this.deactivate;
      return;
    }

    const credentials = {
      email: this.myForm.controls['userName'].value,
      password: this.myForm.controls['userPass'].value
    };

    this.httpRequest.login(credentials).subscribe(
      (data: any) => {
        this.deactivate = !this.deactivate;
        if (data.result === GlobalConst.INTERNAL_SERVER_ERROR || data.status === 500) {
          console.log('login error', 'Internal Server Error');
        } else if (data.result === GlobalConst.RESPONSE_RESULT_SUCCESS) {
          this.ok(data);

        } else if (data.result === GlobalConst.RESPONSE_RESULT_UNAUTHORIZED) {
          this.bad(data.payload);
        } else {
          this.bad(data.result);
        }
      },
      error => {
        this.deactivate = !this.deactivate;
        this.bad(error);
      });
  }

  ngOnInit() {
  }

  getRegistry() {

   // console.log(this.activatedRoute.snapshot);
    this.router.navigate(['registry'], {relativeTo: this.activatedRoute});

    /* this.router.navigate(['registry'], {
       queryParams: {
         returnUrl:urlprimary
       },
       relativeTo: this.activatedRoute
     })*/

  }

  resetForm() {
    this.userServerError = '';
    this.myForm.controls['userName'].setValue('');
    this.myForm.controls['userPass'].setValue('');
    this.submitAttempt = false;
  }

}
