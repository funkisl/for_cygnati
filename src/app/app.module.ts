import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SharedModule} from "./shared/shared.module";
import {AppRoutes} from "./app.routing";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ConfirmationDialogComponent} from "./common/confirmation-dialog/confirmation-dialog.component";
import {BasketService} from "./services/basket/basket.service";
import {HttpWrapperService} from "./services/http-wrapper/http-wrapper.service";
import {HttpRequestsService} from "./services/http-requests/http-requests.service";
import {PopupService} from "./common/compose-message/popup.service";
import {ComposeMessageComponent} from "./common/compose-message/compose-message.component";
import {LoginComponent} from './main-group/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {Authentication, Authentication4Login} from './common/auth/authentication';
import {CookieService} from 'ngx-cookie-service';



@NgModule({
  declarations: [
    AppComponent,
    ConfirmationDialogComponent,
    ComposeMessageComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutes,
    SharedModule.forRoot()
  ],
  providers: [
    HttpWrapperService,
    HttpRequestsService,
    CookieService,
    BasketService,
    Authentication,
    Authentication4Login,
    PopupService,],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent]
})
export class AppModule {
}
