import { Component, OnInit } from '@angular/core';

export class MenuItem {
  name: string;
  href: string;
}

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  elementsArray: MenuItem[] = [];
  selectedItem: MenuItem;
  currentUserName: string;
  constructor() { }

  ngOnInit() {
    this.elementsArray.push(
      <MenuItem>{name: 'none', href: '/home'}
    );
    this.currentUserName = 'User';
  }

  logout(){

  }
}
