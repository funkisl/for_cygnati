import { Component, OnInit } from '@angular/core';

const EXAMPLELINK: any[] = [
  { link: '/home', name: 'home', icon: 'home' },
  { link: '/simple-desk', name: 'desk', icon: 'grid_on' },
  { link: '/about', name: 'about us', icon: 'help_outline' },
  { link: '/chatWeb', name: 'testing websocket', icon: 'chat' }
];


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public links: any[] = [];
  constructor() { }

  ngOnInit() {
    const linkTemp = JSON.parse(JSON.stringify(EXAMPLELINK));
    this.links = linkTemp.map(link => {
      return link;
    });
  }

  openDialog(){

  }

  setFilter(){

  }

  removeFilter(){

  }

}
