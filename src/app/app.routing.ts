import {RouterModule, Routes} from "@angular/router";
import {MainComponent} from "./shared/layouts/main-component/main.component";
import {ComposeMessageComponent} from "./common/compose-message/compose-message.component";
import {RouterConst} from './globals/RouterConst';
import {LoginComponent} from './main-group/login/login.component';
import {Authentication4Login} from './common/auth/authentication';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: RouterConst.LOGIN, component: LoginComponent, canActivate: [Authentication4Login]},
  {
    path: '', component: MainComponent, children: [
      {path: 'home', loadChildren: './home/home.module#HomeModule'},
      {path: 'simple-desk', loadChildren: './desk/desk.module#DeskModule'},
      {path: 'compose', component: ComposeMessageComponent, outlet: 'popup'},
      {path: 'chatWeb', loadChildren: './chat/chat.module#ChatModule'}
    ]
  }
];

export const AppRoutes = RouterModule.forRoot(routes, {initialNavigation: 'enabled'/*,enableTracing: true*/});
