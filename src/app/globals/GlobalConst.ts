export class GlobalConst {
  static readonly SERVER_ADDR = 'https://staff-eng.com/server/';
//  static readonly SERVER_ADDR = 'http://localhost:7434/';
  static readonly SERVER_IMG_ADDR = 'http://localhost:7434/assets/';
  static readonly Domain ='localhost';
  static readonly RESPONSE_RESULT_SUCCESS = 'OK';
  static readonly RESPONSE_RESULT_ERROR = 'SERVICE_UNAVAILABLE';
  static readonly RESPONSE_RESULT_UNAUTHORIZED = 'UNAUTHORIZED';
  static readonly INTERNAL_SERVER_ERROR = '500';
  static readonly PHONE_MASK = ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  static readonly SIZE_TABLE_BASKET = 10;
  static TOKEN: string;
  static readonly COOKIES_TOKEN = 'tolcom_token';
//  static readonly WEBSOCKET_ADDR = 'ws://localhost:7434/commonNotifications/websocket';
  //static readonly WEBSOCKET_ADDR = 'ws://staff-eng.com/server/commonNotifications/websocket';
  static readonly WEBSOCKET_ADDR = 'wss://staff-eng.com/server/commonNotifications/websocket';
}
