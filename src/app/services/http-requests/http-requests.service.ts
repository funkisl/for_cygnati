import {Injectable} from '@angular/core';
import {HttpWrapperService} from '../http-wrapper/http-wrapper.service';
import {HttpHeaders, HttpParams} from '@angular/common/http';
import {GlobalConst} from '../../globals/GlobalConst';
import {BasketModel} from '../../model/basket-model';
import {SortOrder} from '../../globals/sort-order.enum';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable()
export class HttpRequestsService {

  constructor(private httpWrapper: HttpWrapperService) {
  }


  getAllBasket() {
    return this.httpWrapper.get(GlobalConst.SERVER_ADDR + 'getAllItem');
  }

  getAllItemsByPage(page: number = 0, size :number = GlobalConst.SIZE_TABLE_BASKET, sort: SortOrder = SortOrder.ASCENDING, order: string = 'id') {

    let param = new HttpParams();

    param = param.set('page', page.toString());
    param = param.set('size', size.toString());
    param = param.set('sortOrder', sort.toString());
    param = param.set('sortField', order.toString());

    return this.httpWrapper.getWithParams(GlobalConst.SERVER_ADDR + 'getAllItemByPage', param);
  }


  updateBasket(data: BasketModel) {
    return this.httpWrapper.post(GlobalConst.SERVER_ADDR + 'updateItem', data);
  }

  removeBasket(data: BasketModel) {
    console.log('remove');
    return this.httpWrapper.post(GlobalConst.SERVER_ADDR + 'removeItem', data);
  }

  login(data: any) {
    return this.httpWrapper.post(GlobalConst.SERVER_ADDR + 'login', data);
  }

  // Logout
  logout() {
    return this.httpWrapper.get(GlobalConst.SERVER_ADDR + 'logoutMe');
  }
}
