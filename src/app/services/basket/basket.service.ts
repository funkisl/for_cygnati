import {Injectable} from "@angular/core";
import {HttpRequestsService} from "../http-requests/http-requests.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs/index";
import {BasketModel} from "../../model/basket-model";
import {map} from "rxjs/internal/operators";
import {GlobalConst} from "../../globals/GlobalConst";
import {SortOrder} from '../../globals/sort-order.enum';

@Injectable()
export class BasketService {
  constructor(private httpRequestsService: HttpRequestsService, private router: Router) {
  }
  getAllItems(): Observable<BasketModel[]> {
    return this.httpRequestsService.getAllBasket().pipe(map((data: any) => {
      if (data.result !== GlobalConst.RESPONSE_RESULT_SUCCESS) {
        this.router.navigate(['/']);
        return null;
      }

      return data.payload.map((order: any) => {
        return order;
      });

    }));
  }

  getAllItemsByPage( page: number,size,sort: SortOrder = SortOrder.ASCENDING, order: string = "id"): Observable<any[]> {
    return this.httpRequestsService.getAllItemsByPage(page,size,sort,order).pipe(map((data: any) => {
      if (data.result !== GlobalConst.RESPONSE_RESULT_SUCCESS) {
        this.router.navigate(['/']);
        return null;
      }
      return data;


    /*  return data.payload.content.map((order: any) => {
        return order;
      });*/

    }));
  }

  updateItem(basket: BasketModel): Observable<any> {
    return this.httpRequestsService.updateBasket(basket).pipe(map((data: any) => {
      if ((data.result === GlobalConst.RESPONSE_RESULT_SUCCESS)) {
         return data.result;
      }

      this.router.navigate(['/']);
      return null;
    }));

  }

  removeItem(basket: BasketModel): Observable<any> {
    return this.httpRequestsService.removeBasket(basket).pipe(map((data: any) => {
      if ((data.result === GlobalConst.RESPONSE_RESULT_SUCCESS)) {
        return data.result;
      }

      this.router.navigate(['/']);
      return null;
    }));

  }
}
