import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {GlobalConst} from '../../globals/GlobalConst';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class HttpWrapperService {

  constructor(private http: HttpClient, private cookies: CookieService) {
  }

  getWithoutHeaders(url: string) {
    return this.http.get(url);
  }

  delete(url: string) {
    return this.http.delete(url);
  }

  get(url: string) {
    return this.http.get(url, {headers: this.getHeaders()});
  }

  getJSessions(url: string) {
    return this.http.get(url, {headers: this.getHeaders(), withCredentials: true});
  }

  getWithParams(url: string, params: HttpParams) {
    return this.http.get(url, {headers: this.getHeaders(), params: params});
  }

  post(url: string, body: any) {
    return this.http.post(url, body);
  }

  postJWT(url: string, body: any) {
    return this.http.post(url, body, {headers: this.getHeaders()});
  }

  postJSessions(url: string, body: any) {
    return this.http.post(url, body, {headers: this.getHeaders(), withCredentials: true});
  }

  getHeaders() {
    let token = GlobalConst.TOKEN;
    if (token === undefined) {
      token = this.cookies.get(GlobalConst.COOKIES_TOKEN);
    }
    let headers = new HttpHeaders();
    headers = headers.set('X-AUTH-TOKEN', token ? token : '');
    headers = headers.set('Cross-Origin', 'true');
    return headers;
  }
}
