import { Component, OnInit } from '@angular/core';
import {GlobalConst} from '../globals/GlobalConst';
import {Subscription} from 'rxjs';
import {Message} from '@stomp/stompjs';
import {map} from 'rxjs/operators';
import {InjectableRxStompConfig, RxStompService} from '@stomp/ng2-stompjs';
import {getMyStompConfig} from '../globals/WebSocketConst';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-chat-component',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  title = 'ShiftHacker';
  description = 'websocket + specific user + auth';

  greetings: string[] = [];
  disabled = true;
  name: string;
  message: string;

  // Stream of messages
  private subscriptionStomp: Subscription;
  // Subscription status
  public subscribed: boolean = false;

  constructor(private rxStompService: RxStompService, private cookies: CookieService) {
    const config: InjectableRxStompConfig = getMyStompConfig(this.cookies.get(GlobalConst.COOKIES_TOKEN));
    this.rxStompService.configure(config);
    console.log(this.rxStompService);
    this.rxStompService.activate();


  }

  setConnected(connected: boolean) {
    this.disabled = !connected;

    if (connected) {
      this.greetings = [];
    }
  }

  connect() {
     this.initializeWebSocketConnection();
     this.setConnected(true);
  }

  disconnect() {

  }

  sendName() {
    this.rxStompService.publish({ destination: "/app/send/message", body: JSON.stringify({
        'name': this.message,
        'username': this.name
      }), headers:{login:"test_message"} });
  }

  showGreeting(message) {
    this.greetings.push(message);
    console.log(this.greetings);
  }

  ngOnInit(): void {

  }

  initializeWebSocketConnection() {
    //this.subscriptionStomp = this.rxStompService.watch('/queue/commonSubscribes',{"x-auth-token":"init"}).pipe(map((message: Message) => {
    this.subscriptionStomp = this.rxStompService.watch('/user/queue/messages',{"x-auth-token":"init"}).pipe(map((message: Message) => {
      return message.body;
    })).subscribe((msg_body: string) => {
      console.log(msg_body);
      this.showGreeting(JSON.parse(msg_body).content);
      this.subscribed = true;
    });
  }
}
