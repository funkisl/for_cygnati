import {RouterModule, Routes} from '@angular/router';
import {ChatComponent} from './chat.component';
import {HomeComponent} from '../home/home.component';
import {Authentication} from '../common/auth/authentication';

const routes: Routes = [
  {
    path: '', component: ChatComponent, canActivate: [Authentication],
    data: {
      meta: {
        title: 'chat.title',
        description: 'chat.text',
        override: true
      }
    },
  },
];

export const ChatRoutes = RouterModule.forChild(routes);
