import {RouterModule, Routes} from "@angular/router";
import {NotFoundComponent} from "./not-found.component";

const routes: Routes = [
  {
    path: '', component: NotFoundComponent,
    data: {
      meta: {
        title: 'not-found.title',
        description: 'not-found.text'
      }
    },
  },
];

export const NotFoundRoutes = RouterModule.forChild(routes);
