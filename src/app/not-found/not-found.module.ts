import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {NotFoundRoutes} from "./not-found.routing";
import {NotFoundComponent} from "./not-found.component";

@NgModule({
  imports: [
    CommonModule,
    NotFoundRoutes
  ],
  providers: [],
  declarations: [NotFoundComponent]
})
export class NotFoundModule {
}
