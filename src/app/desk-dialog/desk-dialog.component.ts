import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import * as moment from 'moment';
import {BasketModel, TYPESELECT} from '../model/basket-model';


@Component({
  selector: 'app-desk-dialog',
  templateUrl: './desk-dialog.component.html',
  styleUrls: ['./desk-dialog.component.css']
})
export class DeskDialogComponent implements OnInit {
  typeSelectList = TYPESELECT;
  form: FormGroup;


  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<DeskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BasketModel) {

    this.form = fb.group({
      id: [{value: data.id, disabled: true}],
      item: [data.item, Validators.required],
      dateItem: [data.date],
      description: [data.description, Validators.required],
      amount: [data.amount, Validators.required],
      cost: [data.cost, Validators.required],
      typeItem: [data.type, Validators.required]
    });

  }

  ngOnInit() {

  }


  save() {
    this.dialogRef.close(this.prepareSaveItem());
  }

  close() {
    this.dialogRef.close();
  }


  prepareSaveItem(): BasketModel {
    const formModel = this.form.value;
    const saveItem: BasketModel = {
      id: this.data.id,
      item: formModel.item,
      date: formModel.dateItem,
      type: formModel.typeItem,
      description: formModel.description,
      amount: formModel.amount,
      cost: formModel.cost
    };
    return saveItem;
  }

}

