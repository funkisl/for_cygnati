import {StompConfig, StompService} from '@stomp/ng2-stompjs';
import {GlobalConst} from '../globals/GlobalConst';
import {Injectable} from '@angular/core';

@Injectable()
export class StompRService {
  private config :StompConfig;
  constructor(private _stompService: StompService) { }

  public initStomp() {
   // StompConfig config;
    this.config = this.fetchConfigFromSomeWhere();
    this._stompService.config = this.config;
    this._stompService.initAndConnect();
  }

  private fetchConfigFromSomeWhere() {
    const stompConfig: StompConfig = {
      // Which server?
      url: GlobalConst.WEBSOCKET_ADDR,
      // Headers
      // Typical keys: login, passcode, host
      headers: {
        login: 'guest',
        passcode: 'guest'
        //  "x-auth-token":"8-eyJhbGciOiJIUzUxMiJ9.eyJhdWQiOiIyYzk4NTQ3YzNkYzlmNzU3NDdjMWUxMzNhODBmNmQ3NyIsInN1YiI6ImZ1bmtpc2wxM0BnbWFpbC5jb20iLCJpc3MiOiIwOjA6MDowOjA6MDowOjEiLCJleHAiOjE1NTU0OTEwOTR9.hoqo4LAQdQIGT_92vGRJWTxCR8Dk1akLUtq7Ee6B_JAxm_ALxOb1fqgq6yduepZSffEbjpVQ4Gn-tC2IL2MhPw"
      },
      // How often to heartbeat?
      // Interval in milliseconds, set to 0 to disable
      heartbeat_in: 0, // Typical value 0 - disabled
      heartbeat_out: 20000, // Typical value 20000 - every 20 seconds
      // Wait in milliseconds before attempting auto reconnect
      // Set to 0 to disable
      // Typical value 5000 (5 seconds)
      reconnect_delay: 5000,

      // Will log diagnostics on console
      debug: true
    };
    return undefined;
  }
}
