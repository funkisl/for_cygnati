import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home.component";
import {Authentication} from '../common/auth/authentication';

const routes: Routes = [
  {
    path: '', component: HomeComponent, canActivate: [Authentication],
    data: {
      meta: {
        title: 'home.title',
        description: 'home.text',
        override: true
      }
    },
  },
];

export const HomeRoutes = RouterModule.forChild(routes);
