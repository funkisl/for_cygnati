import {Component, OnInit} from '@angular/core';
import {Message} from '@stomp/stompjs';
import {GlobalConst} from '../globals/GlobalConst';
import {Subscription} from 'rxjs/index';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {InjectableRxStompConfig, RxStompService} from '@stomp/ng2-stompjs';
import {getMyStompConfig} from '../globals/WebSocketConst';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // Stream of messages
  private serverUrl = GlobalConst.SERVER_ADDR + 'commonNotifications';
  private subscriptionStomp: Subscription;
  private stompClient: Observable<Message>;
  // Subscription status
  public subscribed: boolean = false;

  constructor(private rxStompService: RxStompService) {
    const config: InjectableRxStompConfig = getMyStompConfig("" +
      "8-eyJhbGciOiJIUzUxMiJ9.eyJhdWQiOiIyYzk4NTQ3YzNkYzlmNzU3NDdjMWUxMzNhODBmNmQ3NyIsInN1YiI6ImZ1bmtpc2wxM0BnbWFpbC5jb20iLCJpc3MiOiIwOjA6MDowOjA6MDowOjEiLCJleHAiOjE1NTU0OTEwOTR9.hoqo4LAQdQIGT_92vGRJWTxCR8Dk1akLUtq7Ee6B_JAxm_ALxOb1fqgq6yduepZSffEbjpVQ4Gn-tC2IL2MhPw");
   /* this.rxStompService.configure(config);
    console.log(this.rxStompService);
    this.rxStompService.activate();*/

    // you can now publish
   // this.rxStompService.publish({ destination: "/app/sendMessage", body: JSON.stringify({ message: "foo" }), headers:{login:"fff"} });

    // and subscribe
    // return this.rxStompService.watch("/topic/notify", headers).pipe(
    //   tap((message: Message) => {
    //     console.log(message.body);
    //   })
    // );
  }

  ngOnInit() {
 //   this.initializeWebSocketConnection();
  }


  initializeWebSocketConnection() {

   // this.stompClient = this.rxStompService.watch('/queue/commonSubscribes');
    this.subscriptionStomp = this.rxStompService.watch('/queue/commonSubscribes',{"x-auth-token":"ffff"}).pipe(map((message: Message) => {
      return message.body;
    })).subscribe((msg_body: string) => {
      let nameId = JSON.parse(msg_body).receiver;
      let content = JSON.parse(msg_body).content;
      let personName = JSON.parse(msg_body).oper;
      console.log(msg_body);
      this.subscribed = true;
    });
  }

}
