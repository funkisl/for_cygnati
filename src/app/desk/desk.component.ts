import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ConfirmationDialogComponent} from '../common/confirmation-dialog/confirmation-dialog.component';
import {DeskDialogComponent} from '../desk-dialog/desk-dialog.component';
import {BasketService} from '../services/basket/basket.service';
import {BasketModel} from '../model/basket-model';
import {PopupService} from '../common/compose-message/popup.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SortOrder} from '../globals/sort-order.enum';
import {merge, of} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/internal/operators';


export class Total {
  total = 0;
}


@Component({
  selector: 'app-desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.css']
})
export class DeskComponent implements OnInit, AfterViewInit {
  dialogRef: MatDialogRef<ConfirmationDialogComponent>;
  displayedColumns: string[] = ['item', 'date', 'type', 'description', 'amount', 'cost', 'sum', 'actions'];
  dataSource = new MatTableDataSource();

  dataBasket:BasketModel[]=[];
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  columnDefinitions = [
    {def: 'item', showMobile: true},
    {def: 'date', showMobile: true},
    {def: 'type', showMobile: false},
    {def: 'description', showMobile: false},
    {def: 'amount', showMobile: false},
    {def: 'cost', showMobile: false},
    {def: 'sum', showMobile: false},
    {def: 'actions', showMobile: true},
  ];

  constructor(private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              private basketService: BasketService,
              private popupService: PopupService,
              public snackBar: MatSnackBar) {
    this.popupService.setPage('/');
    this.popupService.setRouter(router);
    this.popupService.setActivatedRoute(route);
    console.log("constructor");

  }
  ngOnInit() {
   /* this.paginator.pageSizeOptions =[5,20,50];
    this.paginator.pageSize = 5;*/
    /*this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;*/
    this.paginator.pageSizeOptions =[2,10,50];
    this.paginator.pageSize = 2;
    console.log("ngOnInit");
  }
  ngAfterViewInit() {

    // If the user changes the sort order, reset back to the first page.
    this./*dataSource.*/sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);


    // merge(this./*dataSource.*/sort.sortChange, this./*dataSource.*/paginator.page)
    //   .pipe(
    //     startWith({}),
    //     switchMap(() => {
    //       this.isLoadingResults = true;
    //       return this.basketService.getAllItemsByPage(this.paginator.pageIndex,this.paginator.pageSize,
    //          this.sort.direction  === "asc" ? SortOrder.ASCENDING : SortOrder.DESCENDING ,this.sort.active);
    //     }),
    //     map(data => {
    //       // Flip flag to show that loading has finished.
    //       this.isLoadingResults = false;
    //       this.isRateLimitReached = false;
    //
    //       this.resultsLength = data.payload.totalElements;
    //      // this./*dataSource.*/paginator.length = data.payload.totalElements;
    //       console.log(this.resultsLength);
    //       console.log(data);
    //
    //       return data.payload.content;
    //     }),
    //     catchError(() => {
    //       this.isLoadingResults = false;
    //       // Catch if the GitHub API has reached its rate limit. Return empty data.
    //       this.isRateLimitReached = true;
    //       console.log("error?");
    //
    //       return of([]);
    //     })
    //   ).subscribe(data => this./*dataSource.*/dataBasket = data);
    //


    //this.getAllItem();
  }

  getAllItem() {
    this.basketService.getAllItems().subscribe((data: any) => {
     // this.dataSource.data = data;
      this.dataSource.data = data;
    });

  }

  getAllItem_() {
   /* this.basketService.getAllItemsByPage(0,2,SortOrder.ASCENDING,"id" ).subscribe((data: any) => {
    //  this.dataSource.data = data;
      this.dataSource.data = data;
    });*/
   this.paginator._changePageSize(this.paginator.pageSize);

  }
  getTotalCost() {
    return this./*dataSource.*/dataBasket.map(t => t).reduce((acc, value) => acc + value.amount * value.cost, 0);
  }

  openDialog(basket: BasketModel) {

    this.dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      disableClose: false
    });
    this.dialogRef.componentInstance.confirmMessage = `Remove ${basket.item}`;
    this.dialogRef.afterClosed().subscribe(ok => {
      if (ok) {
        this.basketService.removeItem(basket).subscribe((data: any) => {
          this.openSnackBar('removal was successful', 'Ok');
          this.getAllItem();
        });
      }
      this.dialogRef = null;
    });

  }


  openDeskDialog(basket: BasketModel | undefined) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.maxHeight = '800px';
    dialogConfig.minHeight = '350px';
    dialogConfig.maxWidth = '500px';
    dialogConfig.minWidth = '350px';
    dialogConfig.data = basket === undefined ? new BasketModel() : basket;


    const dialogRef = this.dialog.open(DeskDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      val => {
        if (!val) {
          return;
        }

        this.basketService.updateItem(val).subscribe(id => {
          // this.popupService.popupView(`Successfull`, `Save complete`, 5);
          this.openSnackBar('Successfull', 'Ok');
          this.getAllItem();
        });

      }
    );
  }

  getDisplayedColumns(): string[] {
    const isMobile = window.innerWidth < 576;
    return this.columnDefinitions
      .filter(cd => !isMobile || cd.showMobile)
      .map(cd => cd.def);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
