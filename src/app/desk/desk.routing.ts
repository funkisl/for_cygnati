import {RouterModule, Routes} from "@angular/router";
import {DeskComponent} from "./desk.component";


const routes: Routes = [
  {
    path: '', component: DeskComponent,
    data: {
      meta: {
        title: 'home.title',
        description: 'home.text',
        override: true
      }
    },
  },
];

export const DeskRoutes = RouterModule.forChild(routes);
