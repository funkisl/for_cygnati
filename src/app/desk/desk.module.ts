import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DeskComponent} from "./desk.component";
import {DeskRoutes} from "./desk.routing";
import {MaterialsModule} from "../../materials.module";
import {DeskDialogComponent} from "../desk-dialog/desk-dialog.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";







@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DeskRoutes,
    MaterialsModule
  ],
  declarations: [DeskComponent,DeskDialogComponent],
  entryComponents:[DeskDialogComponent]
})
export class DeskModule {
}
